package com.shokwork.testhilt

import com.shokwork.testhilt.app.TestHiltApp

object Commons {

    fun getString(string: Int): String = TestHiltApp.appContext.getString(string)
}