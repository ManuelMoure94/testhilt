package com.shokwork.testhilt

interface RemoteErrorEmitter {
    fun onError(msg: String)
}