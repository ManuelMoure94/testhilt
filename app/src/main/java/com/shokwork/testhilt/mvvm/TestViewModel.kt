package com.shokwork.testhilt.mvvm

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.shokwork.testhilt.RemoteErrorEmitter
import com.shokwork.testhilt.app.network.HardwareItem
import com.shokwork.testhilt.repositories.TestRepository
import com.shokwork.testhilt.repositories.managers.SessionManager
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class TestViewModel @Inject constructor(
    private val repository: TestRepository,
    private val sessionManager: SessionManager
) : ViewModel() {

    fun test() {
        viewModelScope.launch(Dispatchers.Main) {
            val response = repository.getHardwareListNon(
                errorEmitter = object : RemoteErrorEmitter {
                    override fun onError(msg: String) {
                        Log.d("error_325", msg)
                    }
                })
        }
    }

    fun test2() {
        viewModelScope.launch(Dispatchers.Main) {
            val response = repository.getChartData(errorEmitter = object : RemoteErrorEmitter {
                override fun onError(msg: String) {

                }
            })

            Log.d("testLog", "token2 -> ${sessionManager.getToken()}")
        }
    }
}