package com.shokwork.testhilt

import android.util.Log
import com.shokwork.testhilt.app.network.ErrorResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Converter
import retrofit2.HttpException
import retrofit2.Response
import retrofit2.Retrofit
import java.io.IOException
import java.net.SocketTimeoutException
import javax.inject.Inject

open class BaseRepository {

    @Inject
    lateinit var retrofit: Retrofit

    companion object {
        const val TAG = "BaseRemoteRepository"
        const val MESSAGE_KEY = "message"
        const val ERROR_KEY = "error"
    }

    /**
     * Function that executes the given function on Dispatchers.IO context and switch to Dispatchers.Main context when an error occurs
     * @param callFunction is the function that is returning the wanted object. It must be a suspend function. Eg:
     * override suspend fun loginUser(body: LoginUserBody, emitter: RemoteErrorEmitter): LoginUserResponse?  = safeApiCall( { authApi.loginUser(body)} , emitter)
     * @param emitter is the interface that handles the error messages. The error messages must be displayed on the MainThread, or else they would throw an Exception.
     */
    suspend inline fun <T> safeApiCall(
        emitter: RemoteErrorEmitter,
        crossinline callFunction: suspend () -> T
    ): T? {
        return try {
            val myObject = withContext(Dispatchers.IO) { callFunction.invoke() }
            myObject
        } catch (e: Exception) {
            getError(e, emitter)
            null
        }
    }

    suspend fun getError(e: Exception, emitter: RemoteErrorEmitter) {
        withContext(Dispatchers.Main) {
            Log.e("BaseRemoteRepo", "Call error: ${e.localizedMessage}", e.cause)
            when (e) {
                is HttpException -> {
                    if (e.code() == 401) emitter.onError("ErrorType.SESSION_EXPIRED")
                    if (e.code() in 500..599) emitter.onError("ErrorType.UNKNOWN")
                    val body = e.response()
                    getErrorMessage(body, emitter)

                }
                is SocketTimeoutException -> emitter.onError("ErrorType.TIMEOUT")
                is IOException -> emitter.onError("ErrorType.NETWORK")
                else -> emitter.onError("ErrorType.UNKNOWN")
            }
        }
    }

    /**
     * Function that executes the given function in whichever thread is given. Be aware, this is not friendly with Dispatchers.IO,
     * since [RemoteErrorEmitter] is intended to display messages to the user about error from the server/DB.
     * @param callFunction is the function that is returning the wanted object. Eg:
     * override suspend fun loginUser(body: LoginUserBody, emitter: RemoteErrorEmitter): LoginUserResponse?  = safeApiCall( { authApi.loginUser(body)} , emitter)
     * @param emitter is the interface that handles the error messages. The error messages must be displayed on the MainThread, or else they would throw an Exception.
     */
    inline fun <T> safeApiCallNoContext(emitter: RemoteErrorEmitter, callFunction: () -> T): T? {
        return try {
            val myObject = callFunction.invoke()
            Log.d("test", "${myObject}")
            myObject
        } catch (e: Exception) {
            e.printStackTrace()
            Log.e("BaseRemoteRepo", "Call error: ${e.localizedMessage}", e.cause)
            when (e) {
                is HttpException -> {
                    if (e.code() == 401) emitter.onError("ErrorType.SESSION_EXPIRED")
                    if (e.code() in 500..599) emitter.onError("ErrorType.UNKNOWN")
                    else {
                        val body = e.response()?.errorBody()
                        emitter.onError(getErrorMessage(body))
                    }
                }
                is SocketTimeoutException -> emitter.onError("ErrorType.TIMEOUT")
                is IOException -> emitter.onError("ErrorType.NETWORK")
                else -> emitter.onError("ErrorType.UNKNOWN")
            }
            null
        }
    }

    fun getErrorMessage(responseBody: ResponseBody?): String {
        return try {
            val jsonObject = JSONObject(responseBody!!.string())
            when {
                jsonObject.has(MESSAGE_KEY) -> jsonObject.getString(MESSAGE_KEY)
                jsonObject.has(ERROR_KEY) -> jsonObject.getString(ERROR_KEY)
                else -> "Something wrong happened"
            }
        } catch (e: Exception) {
            "Something wrong happened"
        }
    }

    private fun getErrorMessage(response: Response<*>?, emitter: RemoteErrorEmitter) {
        if (response == null) {
            emitter.onError("ErrorType.UNKNOWN")
            return
        }

        val errorBody = parseError(response)

        if (errorBody == null) {
            emitter.onError("ErrorType.UNKNOWN")
            return
        } else {
            if (errorBody.propertiesErrors == null) emitter.onError(
                errorBody.error ?: "${errorBody.message}"
            )
        }
    }

    private fun parseError(response: Response<*>): ErrorResponse? {
        return try {
            val converter: Converter<ResponseBody, ErrorResponse> =
                retrofit.responseBodyConverter(ErrorResponse::class.java, arrayOfNulls(0))
            converter.convert(response.errorBody()!!)
        } catch (e: Exception) {
            null
        }
    }
}