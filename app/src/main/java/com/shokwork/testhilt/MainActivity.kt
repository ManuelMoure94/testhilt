package com.shokwork.testhilt

import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.shokwork.testhilt.mvvm.TestViewModel
import com.shokwork.testhilt.repositories.managers.SessionManager
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    @Inject lateinit var sessionManager: SessionManager

    private val quoteViewModel: TestViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Log.d("testLog", "token -> ${sessionManager.getToken()}")
        sessionManager.setToken("esto es una prueba del singleton")

        quoteViewModel.test()
//        quoteViewModel.test2()
    }
}