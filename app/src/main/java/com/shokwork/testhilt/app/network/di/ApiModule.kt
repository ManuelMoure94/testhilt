package com.shokwork.testhilt.app.network.di

import com.shokwork.testhilt.app.network.api.AuthApi
import com.shokwork.testhilt.app.network.api.SecondApi
import com.shokwork.testhilt.app.network.qualifier.QualifierUrl2
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object ApiModule {

    @Provides
    @Singleton
    fun provideLoginApi(retrofit: Retrofit): AuthApi = retrofit.create(AuthApi::class.java)

    @Provides
    @Singleton
    fun provideSecondApi(@QualifierUrl2 retrofit: Retrofit): SecondApi =
        retrofit.create(SecondApi::class.java)
}