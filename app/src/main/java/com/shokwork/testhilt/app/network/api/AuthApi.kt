package com.shokwork.testhilt.app.network.api

import retrofit2.Response
import retrofit2.http.GET

interface AuthApi {

    @GET("hardware/")
    suspend fun getHardwareListNon(): Response<Void?>?
}