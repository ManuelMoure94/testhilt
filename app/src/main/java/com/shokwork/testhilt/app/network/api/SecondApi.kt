package com.shokwork.testhilt.app.network.api

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface SecondApi {

    @GET("timeseries/asset-metrics")
    suspend fun getDataForChart(
        @Query("assets") assets: String,
        @Query("metrics") metrics: String,
        @Query("page_size") page_size: String,
        @Query("pretty") pretty: Boolean
    ): Response<Void?>?
}