package com.shokwork.testhilt.app

import android.app.Application
import android.content.Context
import androidx.multidex.MultiDex
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class TestHiltApp : Application() {

    companion object {

        lateinit var appInstance: TestHiltApp

        val appContext: Context by lazy {
            appInstance.applicationContext
        }
    }

    override fun onCreate() {
        super.onCreate()
        appInstance = this
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }
}