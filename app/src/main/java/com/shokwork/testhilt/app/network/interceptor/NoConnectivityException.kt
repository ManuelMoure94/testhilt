package com.shokwork.testhilt.app.network.interceptor

import com.shokwork.testhilt.Commons
import com.shokwork.testhilt.R
import java.io.IOException

class NoConnectivityException : IOException() {
    override val message: String?
        get() = Commons.getString(R.string.connectivity_exception)
}