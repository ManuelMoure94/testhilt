package com.shokwork.testhilt.app.network.di

import android.content.Context
import android.os.Build
import androidx.multidex.BuildConfig
import com.shokwork.testhilt.app.network.interceptor.ConnectivityInterceptor
import com.shokwork.testhilt.repositories.managers.SessionManager
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import java.io.File
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    private const val OK_HTTP_CACHE = "okhttp_cache"

    @Provides
    @Singleton
    fun providesInterceptor(): HttpLoggingInterceptor {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        return interceptor
    }

    @Provides
    @Singleton
    fun providesCache(cacheFile: File): Cache {
        return Cache(cacheFile, 10 * 1000 * 1000) // 10MB Cache
    }

    @Provides
    @Singleton
    fun providesCacheFile(@ApplicationContext appContext: Context): File {
        return File(appContext.cacheDir, OK_HTTP_CACHE)
    }

    @Provides
    @Singleton
    fun provideOkHttpClient(
        loggingInterceptor: HttpLoggingInterceptor,
        connectivityInterceptor: ConnectivityInterceptor,
        sessionManager: SessionManager,
        cache: Cache
    ): OkHttpClient {
        return OkHttpClient.Builder()
            .addInterceptor(loggingInterceptor)
            .addInterceptor(connectivityInterceptor)
            .addInterceptor { chain ->
                val requestBuilder = chain.request().newBuilder()
                    .addHeader(
                        "User-Agent",
                        "ANDROID BUILD VERSION:${BuildConfig.VERSION_NAME} SMARTPHONE:${Build.MANUFACTURER} ${Build.MODEL} ANDROID VERSION:${Build.VERSION.RELEASE}"
                    )
                    .addHeader("Authorization", "Bearer ${sessionManager.getToken()}")
                    .addHeader("Content-Type", "application/json")
                val request = requestBuilder.build()
                chain.proceed(request)
            }
            .readTimeout(90, TimeUnit.SECONDS)
            .connectTimeout(90, TimeUnit.SECONDS)
            .callTimeout(10, TimeUnit.MINUTES)
            .writeTimeout(10, TimeUnit.MINUTES)
            .cache(cache)
            .build()
    }
}