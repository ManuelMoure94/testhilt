package com.shokwork.testhilt.app.network

data class ErrorResponse(
    val propertiesErrors: Map<String, Array<String?>>? = null,
    val error: String? = null,
    val message: String? = null,
    val url: String? = null,
    val timestamp: Long? = null,
    val ok: Boolean? = null,
    val statusCode: Int? = null
)