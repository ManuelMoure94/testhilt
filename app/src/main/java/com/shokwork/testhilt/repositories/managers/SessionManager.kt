package com.shokwork.testhilt.repositories.managers

import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SessionManager @Inject constructor() {

    private var mToken = ""

    fun getToken() = mToken

    fun setToken(token: String) {
        mToken = token
    }
}