package com.shokwork.testhilt.repositories

import com.shokwork.testhilt.BaseRepository
import com.shokwork.testhilt.RemoteErrorEmitter
import com.shokwork.testhilt.app.network.api.AuthApi
import com.shokwork.testhilt.app.network.api.SecondApi
import retrofit2.Response
import javax.inject.Inject

class TestRepository @Inject constructor(
    private val testApi: AuthApi,
    private val secondApi: SecondApi
) : BaseRepository() {

    suspend fun getHardwareListNon(
        errorEmitter: RemoteErrorEmitter
    ): Response<Void?>? {
        return safeApiCall(errorEmitter) {
            testApi.getHardwareListNon()
        }
    }

    suspend fun getChartData(
        assets: String = "btc",
        metrics: String = "PriceUSD,RevHashRateNtv,RevHashRateUSD",
        page_size: String = "8",
        pretty: Boolean = true,
        errorEmitter: RemoteErrorEmitter
    ): Response<Void?>? {
        return safeApiCall(errorEmitter) {
            secondApi.getDataForChart(assets, metrics, page_size, pretty)
        }
    }
}